# -*- coding: utf-8 -*-

from autocomplete_light import forms
from models import *
from django.forms import CharField
import django

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Stock package"


class StockLocationForm(forms.ModelForm):
    class Meta:
        model = StockLocation
        if django.VERSION >= (1, 6):
            fields = '__all__'


class RestRegisterForm(forms.ModelForm):
    class Meta:
        model = RestRegister
        if django.VERSION >= (1, 6):
            fields = '__all__'


class PriceRegisterForm(forms.ModelForm):
    class Meta:
        model = PriceRegister
        if django.VERSION >= (1, 6):
            fields = '__all__'


class ReserveRegisterForm(forms.ModelForm):
    class Meta:
        model = ReserveRegister
        if django.VERSION >= (1, 6):
            fields = '__all__'


class TransitRegisterForm(forms.ModelForm):
    class Meta:
        model = TransitRegister
        if django.VERSION >= (1, 6):
            fields = '__all__'


class ConsignmentForm(forms.ModelForm):
    class Meta:
        model = Consignment
        if django.VERSION >= (1, 6):
            fields = '__all__'

    doc_no = CharField(required=False)
    code = CharField(required=False)


class RevaluationActForm(forms.ModelForm):
    class Meta:
        model = RevaluationAct
        if django.VERSION >= (1, 6):
            fields = '__all__'

    doc_no = CharField(required=False, )
    code = CharField(required=False)

