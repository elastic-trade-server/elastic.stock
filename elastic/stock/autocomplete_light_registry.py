# -*- coding: utf-8 -*-

import autocomplete_light.shortcuts as autocomplete_light
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Stock package"


autocomplete_light.register(
    Product,
    search_fields=['name', 'sku', ],
    attrs={
        'placeholder': 'Товар....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)

autocomplete_light.register(
    StockLocation,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Место хранения....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)

autocomplete_light.register(
    Party,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Особа....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)
