# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '__first__'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('stock', '0002_auto_20160406_1831'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransitRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
                ('location', models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation')),
                ('product', models.ForeignKey(verbose_name='Product', to='product.Product')),
            ],
            options={
                'verbose_name': 'Transit register',
                'verbose_name_plural': 'Transit registers',
            },
        ),
        migrations.CreateModel(
            name='TransitRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('doc_id', models.PositiveIntegerField()),
                ('doc_content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.TransitRegister')),
            ],
            options={
                'verbose_name': 'Transit register row',
                'verbose_name_plural': 'Transit register rows',
            },
        ),
    ]
