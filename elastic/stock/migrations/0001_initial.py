# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '__first__'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('party', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Doc no', db_index=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code')),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('planned_date', models.DateTimeField(null=True, verbose_name='Planned date', blank=True)),
                ('effective_date', models.DateTimeField(null=True, verbose_name='Effective date', blank=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('from_location_name', models.CharField(max_length=255, verbose_name='From location name')),
                ('from_location_code', models.CharField(max_length=128, verbose_name='From location code')),
                ('to_location_name', models.CharField(max_length=255, verbose_name='To location name')),
                ('to_location_code', models.CharField(max_length=128, verbose_name='To location code')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'WAIT', 'Wait'), (b'FIXED', 'Fixed'), (b'ABORT', 'Abort')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
            ],
            options={
                'verbose_name': 'Consignment',
                'verbose_name_plural': 'Consignments',
            },
        ),
        migrations.CreateModel(
            name='ConsignmentRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product', models.CharField(max_length=1024, verbose_name='Product name')),
                ('product_sku', models.CharField(max_length=128, verbose_name='Product SKU')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('price_cost', models.DecimalField(verbose_name='Price cost', max_digits=15, decimal_places=2)),
                ('total_cost', models.DecimalField(verbose_name='Total cost', max_digits=15, decimal_places=2)),
                ('consignment', models.ForeignKey(related_name='rows', verbose_name='Consignment', to='stock.Consignment')),
            ],
            options={
                'verbose_name': 'Consignment row',
                'verbose_name_plural': 'Consignment rows',
            },
        ),
        migrations.CreateModel(
            name='ReleaseReserveDoc',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Doc no', db_index=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code')),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(null=True, verbose_name='Effective date', blank=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('location_name', models.CharField(max_length=255, verbose_name='Location name')),
                ('location_code', models.CharField(max_length=128, verbose_name='Location code')),
                ('reason', models.CharField(max_length=1024, null=True, verbose_name='Reason', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'FIXED', 'Fixed'), (b'ABORT', 'Abort')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
            ],
            options={
                'verbose_name': 'Release reserve document',
                'verbose_name_plural': 'Release reserve documents',
            },
        ),
        migrations.CreateModel(
            name='ReleaseReserveRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product', models.CharField(max_length=1024, verbose_name='Product name')),
                ('product_sku', models.CharField(max_length=128, verbose_name='Product SKU')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('release_reserve_doc', models.ForeignKey(related_name='rows', verbose_name='Release reserve document', to='stock.ReleaseReserveDoc')),
            ],
            options={
                'verbose_name': 'Row of release reserve document',
                'verbose_name_plural': 'Rows of release reserve document',
            },
        ),
        migrations.CreateModel(
            name='ReserveRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
            ],
            options={
                'verbose_name': 'Reserve register',
                'verbose_name_plural': 'Reserve registers',
            },
        ),
        migrations.CreateModel(
            name='ReserveRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('doc_id', models.PositiveIntegerField()),
                ('doc_content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.ReserveRegister')),
            ],
            options={
                'verbose_name': 'Reserve register row',
                'verbose_name_plural': 'Reserve register rows',
            },
        ),
        migrations.CreateModel(
            name='RestRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
                ('actual_price_cost', models.DecimalField(null=True, verbose_name='Actual price cost', max_digits=15, decimal_places=2, blank=True)),
            ],
            options={
                'verbose_name': 'Rest register',
                'verbose_name_plural': 'Rest registers',
            },
        ),
        migrations.CreateModel(
            name='RestRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('doc_id', models.PositiveIntegerField()),
                ('price_cost', models.DecimalField(null=True, verbose_name='Price cost', max_digits=15, decimal_places=2, blank=True)),
                ('doc_content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.RestRegister')),
            ],
            options={
                'verbose_name': 'Rest register row',
                'verbose_name_plural': 'Rest register rows',
            },
        ),
        migrations.CreateModel(
            name='RevaluationAct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Doc no', db_index=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code')),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(null=True, verbose_name='Effective date', blank=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('location_name', models.CharField(max_length=255, verbose_name='Location name')),
                ('location_code', models.CharField(max_length=128, verbose_name='Location code')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'FIXED', 'Fixed'), (b'ABORT', 'Abort')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
            ],
            options={
                'verbose_name': 'Act of revaluation',
                'verbose_name_plural': 'Acts of revaluation',
            },
        ),
        migrations.CreateModel(
            name='RevaluationActRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product', models.CharField(max_length=1024, verbose_name='Product name')),
                ('product_sku', models.CharField(max_length=128, verbose_name='Product SKU')),
                ('price_cost', models.DecimalField(verbose_name='Price cost', max_digits=15, decimal_places=2)),
                ('revaluation_act', models.ForeignKey(related_name='rows', verbose_name='Act of revaluation', to='stock.RevaluationAct')),
            ],
            options={
                'verbose_name': 'Row of act of revaluation',
                'verbose_name_plural': 'Rows of act of revaluation',
            },
        ),
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('separator', models.CharField(default=b'-', max_length=1, verbose_name='Separator', blank=True)),
                ('prefix', models.CharField(max_length=8, null=True, verbose_name='Prefix', blank=True)),
                ('number', models.IntegerField(default=0, verbose_name='Number', editable=False)),
                ('suffix', models.CharField(max_length=8, null=True, verbose_name='Suffix', blank=True)),
            ],
            options={
                'verbose_name': 'Sequence',
                'verbose_name_plural': 'Sequences',
            },
        ),
        migrations.CreateModel(
            name='StockLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('type_location', models.CharField(default=b'WH', max_length=3, verbose_name='Type location', choices=[(b'WH', 'Warehouse'), (b'CUS', 'Customer'), (b'SUP', 'Supplier'), (b'IN', 'Input'), (b'OUT', 'Output'), (b'STO', 'Store')])),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='Parent', blank=True, to='stock.StockLocation', null=True)),
                ('party', models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True)),
            ],
            options={
                'verbose_name': 'Stock location',
                'verbose_name_plural': 'Stock locations',
            },
        ),
        migrations.AddField(
            model_name='restregister',
            name='location',
            field=models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation'),
        ),
        migrations.AddField(
            model_name='restregister',
            name='product',
            field=models.ForeignKey(verbose_name='Product', to='product.Product'),
        ),
        migrations.AddField(
            model_name='reserveregister',
            name='location',
            field=models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation'),
        ),
        migrations.AddField(
            model_name='reserveregister',
            name='product',
            field=models.ForeignKey(verbose_name='Product', to='product.Product'),
        ),
    ]
