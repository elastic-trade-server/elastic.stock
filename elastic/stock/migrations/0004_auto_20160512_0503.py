# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc
from django.conf import settings


def get_node_name():
    try:
        return getattr(settings, 'ELASTIC_SETTINGS').get('server_name')
    except AttributeError:
        return datetime.datetime(2016, 5, 12, 5, 3, 50, 495669, tzinfo=utc)
    except KeyError:
        return datetime.datetime(2016, 5, 12, 5, 3, 50, 495669, tzinfo=utc)


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0003_transitregister_transitregisterrow'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consignment',
            name='solution',
        ),
        migrations.RemoveField(
            model_name='consignment',
            name='status',
        ),
        migrations.AddField(
            model_name='consignment',
            name='create_by',
            field=models.CharField(
                default=get_node_name(),
                max_length=64,
                verbose_name='Node creator'
            ),
            preserve_default=False,
        ),
    ]
