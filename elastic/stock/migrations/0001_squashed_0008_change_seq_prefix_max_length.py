# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc
import mptt.fields


class Migration(migrations.Migration):

    replaces = [(b'stock', '0001_initial'), (b'stock', '0002_auto_20160406_1831'), (b'stock', '0003_transitregister_transitregisterrow'), (b'stock', '0004_auto_20160512_0503'), (b'stock', '0005_auto_20160513_0733'), (b'stock', '0006_auto_20160801_0211'), (b'stock', '0007_auto_20160802_1040'), (b'stock', '0008_change_seq_prefix_max_length')]

    dependencies = [
        ('product', '__first__'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('product', '0001_initial'),
        ('party', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Doc no', db_index=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code')),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('planned_date', models.DateTimeField(null=True, verbose_name='Planned date', blank=True)),
                ('effective_date', models.DateTimeField(null=True, verbose_name='Effective date', blank=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('from_location_name', models.CharField(max_length=255, verbose_name='From location name')),
                ('from_location_code', models.CharField(max_length=128, verbose_name='From location code')),
                ('to_location_name', models.CharField(max_length=255, verbose_name='To location name')),
                ('to_location_code', models.CharField(max_length=128, verbose_name='To location code')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'WAIT', 'Wait'), (b'FIXED', 'Fixed'), (b'ABORT', 'Abort')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
            ],
            options={
                'verbose_name': 'Consignment',
                'verbose_name_plural': 'Consignments',
            },
        ),
        migrations.CreateModel(
            name='ConsignmentRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product', models.CharField(max_length=1024, verbose_name='Product name')),
                ('product_sku', models.CharField(max_length=128, verbose_name='Product SKU')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('price_cost', models.DecimalField(decimal_places=2, default=0, max_digits=15, blank=True, null=True, verbose_name='Price cost')),
                ('total_cost', models.DecimalField(decimal_places=2, default=0, max_digits=15, blank=True, null=True, verbose_name='Total cost')),
                ('consignment', models.ForeignKey(related_name='rows', verbose_name='Consignment', to='stock.Consignment')),
            ],
            options={
                'verbose_name': 'Consignment row',
                'verbose_name_plural': 'Consignment rows',
            },
        ),
        migrations.CreateModel(
            name='ReserveRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
            ],
            options={
                'verbose_name': 'Reserve register',
                'verbose_name_plural': 'Reserve registers',
            },
        ),
        migrations.CreateModel(
            name='ReserveRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2)),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.ReserveRegister')),
                ('doc_code', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 10, 59, 505056, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('transaction_no', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 5, 957738, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True)),
            ],
            options={
                'verbose_name': 'Reserve register row',
                'verbose_name_plural': 'Reserve register rows',
            },
        ),
        migrations.CreateModel(
            name='RestRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
                ('actual_price_cost', models.DecimalField(null=True, verbose_name='Actual price cost', max_digits=15, decimal_places=2, blank=True)),
            ],
            options={
                'verbose_name': 'Rest register',
                'verbose_name_plural': 'Rest registers',
            },
        ),
        migrations.CreateModel(
            name='RestRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2)),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.RestRegister')),
                ('doc_code', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 8, 942614, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('transaction_no', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 11, 807469, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True)),
            ],
            options={
                'verbose_name': 'Rest register row',
                'verbose_name_plural': 'Rest register rows',
            },
        ),
        migrations.CreateModel(
            name='RevaluationAct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Doc no', db_index=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code')),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(null=True, verbose_name='Effective date', blank=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('location_name', models.CharField(max_length=255, verbose_name='Location name')),
                ('location_code', models.CharField(max_length=128, verbose_name='Location code')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'FIXED', 'Fixed'), (b'ABORT', 'Abort')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
            ],
            options={
                'verbose_name': 'Act of revaluation',
                'verbose_name_plural': 'Acts of revaluation',
            },
        ),
        migrations.CreateModel(
            name='RevaluationActRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product', models.CharField(max_length=1024, verbose_name='Product name')),
                ('product_sku', models.CharField(max_length=128, verbose_name='Product SKU')),
                ('price_cost', models.DecimalField(verbose_name='Price cost', max_digits=15, decimal_places=2)),
                ('revaluation_act', models.ForeignKey(related_name='rows', verbose_name='Act of revaluation', to='stock.RevaluationAct')),
            ],
            options={
                'verbose_name': 'Row of act of revaluation',
                'verbose_name_plural': 'Rows of act of revaluation',
            },
        ),
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('separator', models.CharField(default=b'-', max_length=1, verbose_name='Separator', blank=True)),
                ('prefix', models.CharField(max_length=32, null=True, verbose_name='Prefix', blank=True)),
                ('number', models.IntegerField(default=0, verbose_name='Number', editable=False)),
                ('suffix', models.CharField(max_length=8, null=True, verbose_name='Suffix', blank=True)),
            ],
            options={
                'verbose_name': 'Sequence',
                'verbose_name_plural': 'Sequences',
            },
        ),
        migrations.CreateModel(
            name='StockLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('type_location', models.CharField(default=b'WH', max_length=3, verbose_name='Type location', choices=[(b'WH', 'Warehouse'), (b'CUS', 'Customer'), (b'SUP', 'Supplier'), (b'IN', 'Input'), (b'OUT', 'Output'), (b'STO', 'Store')])),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='Parent', blank=True, to='stock.StockLocation', null=True)),
                ('party', models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True)),
            ],
            options={
                'verbose_name': 'Stock location',
                'verbose_name_plural': 'Stock locations',
            },
        ),
        migrations.AddField(
            model_name='restregister',
            name='location',
            field=models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation'),
        ),
        migrations.AddField(
            model_name='restregister',
            name='product',
            field=models.ForeignKey(verbose_name='Product', to='product.Product'),
        ),
        migrations.AddField(
            model_name='reserveregister',
            name='location',
            field=models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation'),
        ),
        migrations.AddField(
            model_name='reserveregister',
            name='product',
            field=models.ForeignKey(verbose_name='Product', to='product.Product'),
        ),
        migrations.CreateModel(
            name='TransitRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_quantity', models.DecimalField(default=0, null=True, verbose_name='Actual quantity', max_digits=12, decimal_places=3)),
                ('location', models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation')),
                ('product', models.ForeignKey(verbose_name='Product', to='product.Product')),
            ],
            options={
                'verbose_name': 'Transit register',
                'verbose_name_plural': 'Transit registers',
            },
        ),
        migrations.CreateModel(
            name='TransitRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2)),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.TransitRegister')),
                ('doc_code', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 18, 187718, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('transaction_no', models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 20, 968463, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True)),
            ],
            options={
                'verbose_name': 'Transit register row',
                'verbose_name_plural': 'Transit register rows',
            },
        ),
        migrations.RemoveField(
            model_name='consignment',
            name='solution',
        ),
        migrations.RemoveField(
            model_name='consignment',
            name='status',
        ),
        migrations.AddField(
            model_name='consignment',
            name='created_by',
            field=models.CharField(default=b'dev1', max_length=64, verbose_name='Node creator'),
        ),
        migrations.RemoveField(
            model_name='revaluationact',
            name='solution',
        ),
        migrations.RemoveField(
            model_name='revaluationact',
            name='status',
        ),
        migrations.AddField(
            model_name='revaluationact',
            name='created_by',
            field=models.CharField(default=b'dev1', max_length=64, verbose_name='Node creator'),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='PriceRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_price_cost', models.DecimalField(null=True, verbose_name='Actual price cost', max_digits=15, decimal_places=2, blank=True)),
                ('location', models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation')),
                ('product', models.ForeignKey(verbose_name='Product', to='product.Product')),
            ],
            options={
                'verbose_name': 'Price register',
                'verbose_name_plural': 'Price registers',
            },
        ),
        migrations.CreateModel(
            name='PriceRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('price_cost', models.DecimalField(null=True, verbose_name='Price cost', max_digits=15, decimal_places=2, blank=True)),
                ('transaction_no', models.CharField(max_length=128, verbose_name='Transaction No', db_index=True)),
                ('doc_code', models.CharField(max_length=128, verbose_name='Document internal code', db_index=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.PriceRegister')),
            ],
            options={
                'verbose_name': 'Price register row',
                'verbose_name_plural': 'Price register rows',
            },
        ),
        migrations.RemoveField(
            model_name='restregister',
            name='actual_price_cost',
        ),
        migrations.AlterField(
            model_name='consignment',
            name='code',
            field=models.CharField(max_length=128, verbose_name='Code', db_index=True),
        ),
        migrations.AlterField(
            model_name='revaluationact',
            name='code',
            field=models.CharField(max_length=128, verbose_name='Code', db_index=True),
        ),
    ]
