# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0006_auto_20160801_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consignment',
            name='code',
            field=models.CharField(max_length=128, verbose_name='Code', db_index=True),
        ),
        migrations.AlterField(
            model_name='revaluationact',
            name='code',
            field=models.CharField(max_length=128, verbose_name='Code', db_index=True),
        ),
    ]
