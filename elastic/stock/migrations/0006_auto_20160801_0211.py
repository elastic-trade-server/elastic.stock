# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
        ('stock', '0005_auto_20160513_0733'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceRegister',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actual_datetime', models.DateTimeField(verbose_name='Actual date and time')),
                ('actual_price_cost', models.DecimalField(null=True, verbose_name='Actual price cost', max_digits=15, decimal_places=2, blank=True)),
                ('location', models.ForeignKey(verbose_name='Stock location', to='stock.StockLocation')),
                ('product', models.ForeignKey(verbose_name='Product', to='product.Product')),
            ],
            options={
                'verbose_name': 'Price register',
                'verbose_name_plural': 'Price registers',
            },
        ),
        migrations.CreateModel(
            name='PriceRegisterRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('effective_date', models.DateTimeField(verbose_name='Effective date')),
                ('price_cost', models.DecimalField(null=True, verbose_name='Price cost', max_digits=15, decimal_places=2, blank=True)),
                ('transaction_no', models.CharField(max_length=128, verbose_name='Transaction No', db_index=True)),
                ('doc_code', models.CharField(max_length=128, verbose_name='Document internal code', db_index=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('register', models.ForeignKey(related_name='rows', verbose_name='Register', to='stock.PriceRegister')),
            ],
            options={
                'verbose_name': 'Price register row',
                'verbose_name_plural': 'Price register rows',
            },
        ),
        migrations.RemoveField(
            model_name='reserveregisterrow',
            name='doc_content_type',
        ),
        migrations.RemoveField(
            model_name='reserveregisterrow',
            name='doc_id',
        ),
        migrations.RemoveField(
            model_name='restregister',
            name='actual_price_cost',
        ),
        migrations.RemoveField(
            model_name='restregisterrow',
            name='doc_content_type',
        ),
        migrations.RemoveField(
            model_name='restregisterrow',
            name='doc_id',
        ),
        migrations.RemoveField(
            model_name='restregisterrow',
            name='price_cost',
        ),
        migrations.RemoveField(
            model_name='transitregisterrow',
            name='doc_content_type',
        ),
        migrations.RemoveField(
            model_name='transitregisterrow',
            name='doc_id',
        ),
        migrations.AddField(
            model_name='reserveregisterrow',
            name='doc_code',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 10, 59, 505056, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reserveregisterrow',
            name='short_desc',
            field=models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True),
        ),
        migrations.AddField(
            model_name='reserveregisterrow',
            name='transaction_no',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 5, 957738, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='restregisterrow',
            name='doc_code',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 8, 942614, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='restregisterrow',
            name='short_desc',
            field=models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True),
        ),
        migrations.AddField(
            model_name='restregisterrow',
            name='transaction_no',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 11, 807469, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transitregisterrow',
            name='doc_code',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 18, 187718, tzinfo=utc), max_length=128, verbose_name='Document internal code', db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transitregisterrow',
            name='short_desc',
            field=models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True),
        ),
        migrations.AddField(
            model_name='transitregisterrow',
            name='transaction_no',
            field=models.CharField(default=datetime.datetime(2016, 8, 1, 2, 11, 20, 968463, tzinfo=utc), max_length=128, verbose_name='Transaction No', db_index=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reserveregisterrow',
            name='quantity',
            field=models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='restregisterrow',
            name='quantity',
            field=models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='transitregisterrow',
            name='quantity',
            field=models.DecimalField(verbose_name='Quantity', max_digits=15, decimal_places=2),
        ),
    ]
