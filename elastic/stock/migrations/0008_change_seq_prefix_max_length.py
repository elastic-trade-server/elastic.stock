# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0007_auto_20160802_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sequence',
            name='prefix',
            field=models.CharField(max_length=32, null=True, blank=True, verbose_name='Prefix')
        )
    ]
