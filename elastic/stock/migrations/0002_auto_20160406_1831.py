# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consignmentrow',
            name='price_cost',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=15, blank=True, null=True, verbose_name='Price cost'),
        ),
        migrations.AlterField(
            model_name='consignmentrow',
            name='total_cost',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=15, blank=True, null=True, verbose_name='Total cost'),
        ),
    ]
