# -*- coding: utf-8 -*-

import django.dispatch


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Stock package"


workflow_restart = django.dispatch.Signal()
workflow_delete = django.dispatch.Signal()
replication_request = django.dispatch.Signal()
