# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from models import *
from mpttadmin.admin import MpttAdmin
from forms import RestRegisterForm, PriceRegisterForm, ReserveRegisterForm, TransitRegisterForm, StockLocationForm, \
    ConsignmentForm, RevaluationActForm
import signals

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Stock package"


def restart_linked_workflow(modeladmin, request, queryset):
    for document in queryset.iterator():
        signals.workflow_restart.send(instance=document, sender=modeladmin)
restart_linked_workflow.short_description = _('Start linked workflow')


def delete_linked_workflow(modeladmin, request, queryset):
    for document in queryset.iterator():
        signals.workflow_delete.send_robust(instance=document, sender=modeladmin)
delete_linked_workflow.short_description = _('Delete linked workflow')


def require_removal_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item, **{'is_removing': True})
require_removal_replication.short_description = _('Distributed request on removal')


def require_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item)
require_replication.short_description = _('Distributed request on update')


class StockLocationAdmin(MpttAdmin):
    class Meta:
        model = StockLocation

    tree_title_field = 'name'
    tree_display = ('name', 'code', 'type_location', )
    form = StockLocationForm

    fieldsets = (
        (None, {
            'fields':  ['parent', 'code', 'name', 'type_location', 'party', ]
        }),
    )

admin.site.register(StockLocation, StockLocationAdmin)


class ConsignmentRowInline(admin.TabularInline):
    model = ConsignmentRow


class ConsignmentAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'creation_date', 'effective_date', 'last_modify_date', 'from_location_name',
                    'to_location_name', 'created_by', )
    list_filter = ('creation_date', 'from_location_name', 'to_location_name', 'created_by', )
    search_fields = ('doc_no', 'code', 'rows__product_sku',)
    date_hierarchy = 'effective_date'
    form = ConsignmentForm
    inlines = [
        ConsignmentRowInline,
    ]
    actions = [require_removal_replication, require_replication, restart_linked_workflow, delete_linked_workflow, ]
    readonly_fields = ('created_by', )

    fieldsets = (
        (None, {
            'fields':  ['doc_no', 'code', 'creation_date', 'planned_date', 'effective_date', 'last_modify_date',
                        'created_by', 'from_location_name', 'from_location_code', 'to_location_name',
                        'to_location_code', 'description', ]
        }),
    )

admin.site.register(Consignment, ConsignmentAdmin)


class RevaluationActRowInline(admin.TabularInline):
    model = RevaluationActRow


class RevaluationActAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'creation_date', 'effective_date', 'location_name', 'created_by', )
    list_filter = ('creation_date', 'location_name', 'created_by', )
    search_fields = ('doc_no', 'code', 'rows__product_sku',)
    date_hierarchy = 'effective_date'
    form = RevaluationActForm
    inlines = [
        RevaluationActRowInline,
    ]
    actions = [require_removal_replication, require_replication, restart_linked_workflow, delete_linked_workflow, ]
    readonly_fields = ('created_by', )

    fieldsets = (
        (None, {
            'fields':  ['doc_no', 'code', 'creation_date', 'effective_date', 'last_modify_date', 'created_by',
                        'location_name', 'location_code', 'description', ]
        }),
    )

admin.site.register(RevaluationAct, RevaluationActAdmin)


class RestRegisterRowInline(admin.TabularInline):
    model = RestRegisterRow
    readonly_fields = ('creation_date', 'effective_date', 'quantity', 'short_desc', 'doc_code', 'transaction_no', )

    def get_readonly_fields(self, request, obj=None):
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class RestRegisterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'product_sku', 'manufacturer', 'location', 'actual_quantity', )
    search_fields = ('location__code', 'product__sku',)
    list_filter = ('location', )
    readonly_fields = ('actual_datetime', 'location', 'actual_quantity', 'product', )
    form = RestRegisterForm

    inlines = [
        RestRegisterRowInline,
    ]

    def get_readonly_fields(self, request, obj=None):
        if not obj:  # Новая запись. Все поля доступны для редактирование
            return ()
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

admin.site.register(RestRegister, RestRegisterAdmin)


class PriceRegisterRowInline(admin.TabularInline):
    model = PriceRegisterRow
    readonly_fields = ('creation_date', 'effective_date', 'price_cost', 'short_desc', 'doc_code', 'transaction_no', )

    def get_readonly_fields(self, request, obj=None):
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class PriceRegisterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'product_sku', 'manufacturer', 'location', 'actual_price_cost', )
    search_fields = ('location__code', 'product__sku',)
    list_filter = ('location', )
    readonly_fields = ('actual_datetime', 'location', 'actual_price_cost', 'product', )
    form = PriceRegisterForm

    inlines = [
        PriceRegisterRowInline,
    ]

    def get_readonly_fields(self, request, obj=None):
        if not obj:  # Новая запись. Все поля доступны для редактирование
            return ()
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

admin.site.register(PriceRegister, PriceRegisterAdmin)


class ReserveRegisterRowInline(admin.TabularInline):
    model = ReserveRegisterRow
    readonly_fields = ('creation_date', 'effective_date', 'quantity', 'short_desc', 'doc_code', 'transaction_no', )

    def get_readonly_fields(self, request, obj=None):
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ReserveRegisterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'product_sku', 'manufacturer', 'location', 'actual_quantity', )
    search_fields = ('location__code', 'product__sku',)
    list_filter = ('location', )
    readonly_fields = ('actual_datetime', 'location', 'actual_quantity', 'product', )
    form = ReserveRegisterForm

    inlines = [
        ReserveRegisterRowInline,
    ]

    def get_readonly_fields(self, request, obj=None):
        if not obj:  # Новая запись. Все поля доступны для редактирование
            return ()
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()


admin.site.register(ReserveRegister, ReserveRegisterAdmin)


class TransitRegisterRowInline(admin.TabularInline):
    model = TransitRegisterRow
    readonly_fields = ('creation_date', 'effective_date', 'quantity', 'short_desc', 'doc_code', 'transaction_no', )

    def get_readonly_fields(self, request, obj=None):
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class TransitRegisterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'product_sku', 'manufacturer', 'location', 'actual_quantity', )
    search_fields = ('location__code', 'product__sku',)
    list_filter = ('location', )
    readonly_fields = ('actual_datetime', 'location', 'actual_quantity', 'product', )
    form = TransitRegisterForm

    inlines = [
        TransitRegisterRowInline,
    ]

    def get_readonly_fields(self, request, obj=None):
        if not obj:  # Новая запись. Все поля доступны для редактирование
            return ()
        # Если в настройках товара указано что он контролируются товароучеткой
        if obj and obj.product and obj.product.is_synchronized:
            # ....то запрещаем менять значения регистров через административный интерфейс
            return self.readonly_fields

        # Во всех остальных случаях разрешаем редактировать поля регистров
        return ()

admin.site.register(TransitRegister, TransitRegisterAdmin)


def request_next_number(modeladmin, request, queryset):
    for document in queryset.iterator():
        document.request_next_number()
request_next_number.short_description = _('Request next number')


class SequenceAdmin(admin.ModelAdmin):
    list_display = ('name', 'current_number', )
    readonly_fields = ('number', )

    actions = [request_next_number, ]

admin.site.register(Sequence, SequenceAdmin)
