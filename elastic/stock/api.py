# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.bundle import Bundle
from tastypie.utils.urls import trailing_slash
from tastypie.exceptions import BadRequest
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from dateutil.parser import parse
from tastypie.utils import make_aware
import signals


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Stock package"


class StockLocationResource(ModelResource):
    class Meta:
        queryset = StockLocation.objects.all()
        resource_name = 'location'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', 'lft', 'level', 'tree_id', 'rght', ]
        allowed_methods = ['get', ]
        filtering = {
            "name": ALL,
            "code": ALL,
            "party": ALL_WITH_RELATIONS,
        }

    parent = fields.ToOneField('self', attribute='parent', null=True, blank=True, full=False, help_text=_('Parent'))
    code = fields.CharField(attribute='code', help_text=_('Code'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    type_location = fields.CharField(attribute='type_location', help_text=_('Type'))
    party = fields.ToOneField('elastic.party.api.PartyResource', attribute='party', null=True, blank=True, full=True,
                              help_text=_('Party'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs


class ConsignmentRowResource(ModelResource):
    class Meta:
        queryset = ConsignmentRow.objects.all()
        resource_name = 'consignment_row'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]

    product = fields.CharField(attribute='product', help_text=_('Product name'))
    product_sku = fields.CharField(attribute='product_sku', help_text=_('Product SKU'))
    quantity = fields.DecimalField(attribute='quantity', help_text=_('Quantity'))
    price_cost = fields.DecimalField(attribute='price_cost', help_text=_('Price cost'))
    total_cost = fields.DecimalField(attribute='total_cost', help_text=_('Total cost'))
    consignment = fields.ToOneField("elastic.stock.api.ConsignmentResource", 'consignment', help_text=_('Consignment'))

    def dehydrate(self, bundle):
        bundle.data.pop('consignment')
        return bundle


class ConsignmentResource(ModelResource):
    class Meta:
        queryset = Consignment.objects.all()
        resource_name = 'consignment'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        list_allowed_methods = ['get', 'post', ]
        detail_allowed_methods = ['get', 'post', 'delete', ]
        include_resource_uri = False
        excludes = ['id', ]
        filtering = {
            "doc_no": ALL,
            "creation_date": ALL,
            "planned_date": ALL,
            "effective_date": ALL,
        }

    doc_no = fields.CharField(attribute='doc_no', help_text=_('Document no'))
    code = fields.CharField(attribute='code', help_text=_('Document internal code'))
    creation_date = fields.DateTimeField(attribute='creation_date', help_text=_('Creation date'))
    planned_date = fields.DateTimeField(attribute='planned_date', null=True, blank=True, help_text=_('Planned date'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    effective_date = fields.DateTimeField(attribute='effective_date', null=True, blank=True,
                                          help_text=_('Effective date'))
    created_by = fields.CharField(attribute='created_by', null=True, blank=True, help_text=_('Created by'))
    from_location_name = fields.CharField(attribute='from_location_name', help_text=_('From location (name)'))
    from_location_code = fields.CharField(attribute='from_location_code', help_text=_('From location (code)'))
    to_location_name = fields.CharField(attribute='to_location_name', help_text=_('To location (name)'))
    to_location_code = fields.CharField(attribute='to_location_code', help_text=_('To location (code)'))
    description = fields.CharField(attribute='description', null=True, blank=True, help_text=_('Description'))
    rows = fields.ToManyField(ConsignmentRowResource, attribute='rows', related_name='consignment', full=True,
                              null=True, blank=True, help_text=_('Consignment rows'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/status%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_doc_status'), name="api_get_doc_status"),
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def get_doc_status(self, request, code, **kwargs):
        """
        Returns a serialized document status.

        Should return a HttpResponse (200 OK).
        """
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        self.log_throttled_access(request)
        bundle = self.build_bundle(obj=self.Meta.queryset.get(code=code), request=request)
        self.authorized_read_detail(self.get_object_list(bundle.request), bundle)

        data = {
            "state_name": bundle.obj.get_workflow().get_current_state_name(),
            "state_code": bundle.obj.get_workflow().get_current_state_code(),
        }

        return self.create_response(request, data)

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    @staticmethod
    def hydrate_created_by(bundle):
        if bundle.data.get('sync', True):
            # Устанавливаем имя локального нода, в качестве нода-создателя документа
            bundle.obj.created_by = settings.ELASTIC_SETTINGS['server_name']
        else:
            bundle.obj.created_by = bundle.data.get('created_by', None)
        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = Consignment.objects.get(code=bundle.data.get('code', None))
        except ObjectDoesNotExist:

            bundle = super(ConsignmentResource, self).obj_create(bundle, **kwargs)

            # Продвигаем документ
            signals.workflow_restart.send(instance=bundle.obj, sender=ConsignmentResource)

            if bundle.data.get('sync', True):
                # Запрашиваем репликацию
                signals.replication_request.send(sender=bundle.obj)

            return bundle

        # Обновляем существующий коносамент
        return self.obj_update(bundle, skip_errors=False, **{"code": bundle.obj.code})

    @transaction.atomic
    def obj_update(self, bundle, skip_errors=False, **kwargs):
        try:
            bundle.obj = Consignment.objects.get(code=kwargs.get('code', None))
        except ObjectDoesNotExist:
            raise BadRequest("Consignment with code #{0} not exists!".format(kwargs['code']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        bundle.obj.rows.all().delete()

        bundle = super(ConsignmentResource, self).obj_update(bundle, **kwargs)

        # Продвигаем документ
        signals.workflow_restart.send(instance=bundle.obj, sender=ConsignmentResource)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_delete(self, bundle, **kwargs):
        try:
            consignment = Consignment.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=consignment, **{'is_removing': True})

        consignment.delete()


class RevaluationActRowResource(ModelResource):
    class Meta:
        queryset = RevaluationActRow.objects.all()
        resource_name = 'revaluation_act_row'
        authentication = SessionAuthentication()
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]

    product = fields.CharField(attribute='product', help_text=_('Product name'))
    product_sku = fields.CharField(attribute='product_sku', help_text=_('Product SKU'))
    price_cost = fields.DecimalField(attribute='price_cost', help_text=_('New price cost'))
    revaluation_act = fields.ToOneField("elastic.stock.api.RevaluationActResource", 'revaluation_act',
                                        help_text=_('Consignment'))

    def dehydrate(self, bundle):
        bundle.data.pop('revaluation_act')
        return bundle


class RevaluationActResource(ModelResource):
    class Meta:
        queryset = RevaluationAct.objects.all()
        resource_name = 'revaluation_act'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        list_allowed_methods = ['get', 'post', ]
        detail_allowed_methods = ['get', 'post', 'delete', ]
        include_resource_uri = False
        excludes = ['id', ]
        filtering = {
            "doc_no": ALL,
            "creation_date": ALL,
            "effective_date": ALL,
        }

    doc_no = fields.CharField(attribute='doc_no', help_text=_('Document no'))
    code = fields.CharField(attribute='code', help_text=_('Document internal code'))
    creation_date = fields.DateTimeField(attribute='creation_date', help_text=_('Creation date'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    effective_date = fields.DateTimeField(attribute='effective_date', null=True, blank=True,
                                          help_text=_('Effective date'))
    created_by = fields.CharField(attribute='created_by', null=True, blank=True, help_text=_('Created by'))
    location_name = fields.CharField(attribute='location_name', help_text=_('Location (name)'))
    location_code = fields.CharField(attribute='location_code', help_text=_('Location (code)'))
    description = fields.CharField(attribute='description', null=True, blank=True, help_text=_('Description'))
    rows = fields.ToManyField(RevaluationActRowResource, attribute='rows', related_name='revaluation_act', full=True,
                              null=True, blank=True, help_text=_('Rows of act of revaluation'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/status%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_doc_status'), name="api_get_doc_status"),
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def get_doc_status(self, request, code, **kwargs):
        """
        Returns a serialized document status.

        Should return a HttpResponse (200 OK).
        """
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        self.log_throttled_access(request)
        bundle = self.build_bundle(obj=self.Meta.queryset.get(code=code), request=request)
        self.authorized_read_detail(self.get_object_list(bundle.request), bundle)

        data = {
            "state_name": bundle.obj.get_workflow().get_current_state_name(),
            "state_code": bundle.obj.get_workflow().get_current_state_code(),
        }

        return self.create_response(request, data)

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    @staticmethod
    def hydrate_created_by(bundle):
        if bundle.data.get('sync', True):
            # Устанавливаем имя локального нода, в качестве нода-создателя документа
            bundle.obj.created_by = settings.ELASTIC_SETTINGS['server_name']
        else:
            bundle.obj.created_by = bundle.data.get('created_by', None)
        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = RevaluationAct.objects.get(code=bundle.data.get('code', None))
        except ObjectDoesNotExist:

            bundle = super(RevaluationActResource, self).obj_create(bundle, **kwargs)

            # Продвигаем документ
            signals.workflow_restart.send(instance=bundle.obj, sender=ConsignmentResource)

            if bundle.data.get('sync', True):
                # Запрашиваем репликацию
                signals.replication_request.send(sender=bundle.obj)

            return bundle

        # Обновляем существующий акт
        return self.obj_update(bundle, skip_errors=False, **{"code": bundle.obj.code})

    @transaction.atomic
    def obj_update(self, bundle, skip_errors=False, **kwargs):
        try:
            bundle.obj = RevaluationAct.objects.get(code=bundle.data.get('code', None))
        except ObjectDoesNotExist:
            raise BadRequest("Revaluation act with code #{0} not exists!".format(kwargs['code']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        bundle.obj.rows.all().delete()

        bundle = super(RevaluationActResource, self).obj_update(bundle, **kwargs)

        # Продвигаем документ
        signals.workflow_restart.send(instance=bundle.obj, sender=ConsignmentResource)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_delete(self, bundle, **kwargs):
        try:
            act = RevaluationAct.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=act, **{'is_removing': True})

        act.delete()


class StockRestRegisterResource(ModelResource):
    class Meta:
        queryset = RestRegister.objects.all()
        resource_name = 'stock_rest_register'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = ReadOnlyAuthorization()
        include_resource_uri = False
        allowed_methods = ['get', ]
        filtering = {
            "product": ALL_WITH_RELATIONS,
            "location": ALL_WITH_RELATIONS,
        }
        max_limit = 0

    actual_datetime = fields.DateTimeField(attribute="actual_datetime", help_text=_("Actual date and time"))
    actual_price_cost = fields.DecimalField(attribute='actual_price_cost', null=True, blank=True,
                                            help_text=_('Actual price cost'))
    actual_quantity = fields.DecimalField(attribute='actual_quantity', null=True, blank=False,
                                          help_text=_('Actual quantity'))
    location = fields.ForeignKey(to='elastic.stock.api.StockLocationResource', attribute='location',
                                 full=True, help_text=_('Stock location'))
    product = fields.ToOneField(to="elastic.product.api.ProductResource", full=True, attribute='product',
                                help_text=_('Product'))

    def dehydrate_location(self, bundle):
        if bundle.request.GET.get('view', 'full') == 'short':
            return {"code": bundle.obj.location.code, "name": bundle.obj.location.name}
        return self.location.dehydrate(bundle)

    def dehydrate_product(self, bundle):
        if bundle.request.GET.get('view', 'full') == 'short':
            return {
                "sku": bundle.obj.product.sku,
                "name": bundle.obj.product.name,
                "manufacturer_code": bundle.obj.product.manufacturer_code,
                "manufacturer": bundle.obj.product.manufacturer,
            }
        return self.product.dehydrate(bundle)


class StockReserveRegisterResource(ModelResource):
    class Meta:
        queryset = ReserveRegister.objects.all()
        resource_name = 'stock_reserve_register'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = ReadOnlyAuthorization()
        include_resource_uri = False
        allowed_methods = ['get', ]
        filtering = {
            "product": ALL_WITH_RELATIONS,
            "location": ALL_WITH_RELATIONS,
        }

    actual_datetime = fields.DateTimeField(attribute="actual_datetime", help_text=_("Actual date and time"))
    actual_quantity = fields.DecimalField(attribute='actual_quantity', null=True, blank=False,
                                          help_text=_('Actual quantity'))
    location = fields.ForeignKey(to='elastic.stock.api.StockLocationResource', attribute='location',
                                 full=True, help_text=_('Stock location'))
    product = fields.ToOneField(to="elastic.product.api.ProductResource", full=True, attribute='product',
                                help_text=_('Product'))
